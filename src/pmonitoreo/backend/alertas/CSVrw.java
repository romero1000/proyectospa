package pmonitoreo.backend.alertas;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CSVrw {
    private final String separador;
    private final String ruta;

    public CSVrw(String separador, String ruta){
        this.separador=separador;
        this.ruta=ruta;
    }

    public List<Alerta> leerCSV(){
        List<Alerta> respuesta= new ArrayList<>();
        BufferedReader br=null;
        try {
            br = new BufferedReader(new FileReader(this.ruta));
            String line;
            while ((line = br.readLine()) != null) {
                String [] fila = line.split(this.separador);
                DateFormat fechaHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                Date fechaConFormato = fechaHora.parse(fila[2]);
                respuesta.add(new Alerta(Integer.parseInt(fila[0]),Integer.parseInt(fila[1]),fechaConFormato,fila[3],fila[4]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(br!=null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return respuesta;
    }
    public void escrivirCSV(List<Alerta> alertas){
        try {
            FileWriter writer = new FileWriter(ruta);

            for (Alerta str : alertas) {
                DateFormat fechaHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String convertido = fechaHora.format(str.getFecha());
                writer.append(String.valueOf(str.getId())).append(this.separador).append(String.valueOf(str.getIdCohab())).append(this.separador).append(convertido).append(this.separador).append(str.getStatus()).append(this.separador).append(str.getdescripcion());
                writer.append(System.lineSeparator());
            }
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
