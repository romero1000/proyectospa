package pmonitoreo.backend.alertas;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.*;

public class AlertasServer implements Alertas{
    private List<Alerta> alertas;
    private final CSVrw csv;

    public AlertasServer() {
        this.csv=new CSVrw(",", "alertas.csv");
        this.alertas=csv.leerCSV();
        leerConJsonSimple();
    }

    private void leerConJsonSimple(){
        JSONParser parser = new JSONParser();
        try{
            Object obj = parser.parse(new FileReader("agenteConf.json"));
            JSONObject jsonObject = (JSONObject) obj;
            System.out.println("Usuario conectado: "+jsonObject.get("usuario"));
        }catch (Exception e){
            System.out.println("Hubo un problema en: "+e);
        }
    }

    @Override
    public void eliminarAlerta(int id) {
        for (int i=0 ;i< this.alertas.size(); i++){
            if(this.alertas.get(i).getIdCohab()==id){
                this.alertas.remove(i);
            }
        }
        this.csv.escrivirCSV(this.alertas);
    }

    @Override
    public void agregarAlerta(Alerta x) {
        this.alertas.add(0,x);
        this.csv.escrivirCSV(this.alertas);
    }

    @Override
    public List<Alerta> getAlertas() {
        this.alertas=csv.leerCSV();
        return this.alertas;
    }

    @Override
    public List<Alerta> getAlertasxCohab(int id) {
        List<Alerta> alertasPorCohabitante= new ArrayList<>();
        int idcohabitante;
        for(Alerta a: this.alertas){
            idcohabitante=a.getIdCohab();
            if(idcohabitante == id){
                alertasPorCohabitante.add(a);
            }
        }
        return alertasPorCohabitante;
    }
}
