package pmonitoreo.asanitario;

import javax.swing.*;

public class Acciones {
    private JButton btn_select;
    private JButton btn_alertas;
    private JTextField input;
    private JButton btn_analisis_automatico;

    private JComboBox<String> jComboBox1;
    private JComboBox<String> jComboBox2;

    public Acciones(AgenteGUI agente){
        inicializarBotones();
        asignarEscuchadores(agente);
        darEstilos();
        asignarValoresAComboboxes();
    }
    private void asignarEscuchadores(AgenteGUI agente){
        this.jComboBox1.addActionListener(agente);
        this.jComboBox2.addActionListener(agente);
        this.btn_select.addActionListener(agente);
        this.btn_alertas.addActionListener(agente);
        this.btn_analisis_automatico.addActionListener(agente);
    }
    private void inicializarBotones(){
        this.jComboBox1 = new JComboBox<>();
        this.jComboBox2 = new JComboBox<>();
        this.btn_select = new JButton("Mostrar");
        this.btn_alertas = new JButton("Ver Alertas");
        this.btn_analisis_automatico = new JButton("Realizar analisis");
        this.input=new JTextField(20);
    }
    private void asignarValoresAComboboxes(){
        String [] items = new String[] {"Todos", "Amarillos", "Rojos", "Verdes", "Azules"};
        jComboBox1.setModel(new DefaultComboBoxModel<>(items));
        String [] items2 = new String[] {"Camibiar a Amarilla", "Cambiar a Verde", "Rojo por test positivo"};
        jComboBox2.setModel(new DefaultComboBoxModel<>(items2));
    }
    private void darEstilos(){
        this.input.setBounds(194,325, 145, 25);
        this.btn_alertas.setBounds(340,325, 100, 25);
    }

    public JButton getButtonSelect(){
        return this.btn_select;
    }
    public JTextField getInputDescription(){
        return this.input;
    }
    public JButton getButtonAlertas(){
        return this.btn_alertas;
    }
    public JButton getButtonAnalisis(){
        return this.btn_analisis_automatico;
    }
    public JComboBox<String> getjComboBox1(){
        return this.jComboBox1;
    }
    public JComboBox<String> getjComboBox2(){
        return this.jComboBox2;
    }
}
