package pmonitoreo.asanitario;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import pmonitoreo.backend.alertas.Alerta;
import pmonitoreo.backend.alertas.Alertas;
import pmonitoreo.backend.cohabitantes.Cohabitante;
import pmonitoreo.backend.cohabitantes.Cohabitantes;
import pmonitoreo.backend.cohabitantes.CondicionSanitaria;
import pmonitoreo.backend.cohabitantes.EstadoCohabitante;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class AgenteGUI implements ActionListener{

    private final JFrame ventata;
    private final Cohabitantes cohabitantes;
    private final Alertas alertas;
    private DefaultTableModel dtm;
    private DefaultTableModel dtmt;
    private JTable table;
    private JTable table2;
    private final Acciones acciones;
    private List<Cohabitante> listaInicial;
    private List<Cohabitante> amarillos;
    private List<Cohabitante> rojos;
    private List<Cohabitante> verdes;
    private List<Cohabitante> azules;
    private JLabel message;
    private final String txt;
    private JLabel txt_analisis;

    public AgenteGUI(Cohabitantes cohabitantes, Alertas alertas){
        this.cohabitantes = cohabitantes;
        this.alertas = alertas;
        this.ventata = new JFrame();
        this.acciones = new Acciones(this);
        this.txt = "";
    }
    public void iniciar(){
        this.ventata.setTitle("BIENVENIDO AGENTE SANITARIO");
        this.ventata.setSize(480, 640);
        this.ventata.getContentPane().setBackground(new Color(0x84A5C9));
        this.ventata.setLayout(null);
        crearCombobox();
        crearComboboxAsignation();
        crearTabla();
        crearTablaAsign();
        JPanel btnPanel = new JPanel();
        btnPanel.setBounds(30, 240, 80, 35);
        btnPanel.setBackground(new Color(0x84A5C9));
        btnPanel.add(this.acciones.getButtonSelect());
        crearBotonDeAnalisis();
        this.ventata.add(this.acciones.getInputDescription());
        this.ventata.add(this.acciones.getButtonAlertas());
        messageDates();
        leerConJsonSimple();
        this.ventata.add(btnPanel);
        this.ventata.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.ventata.setVisible(true);
    }

    private void crearCombobox(){
        JPanel conbobox_Panel = new JPanel();
        conbobox_Panel.setBackground(new Color(0x84A5C9));
        conbobox_Panel.setBounds(30, 30, 150, 30);
        conbobox_Panel.add(acciones.getjComboBox1());
        this.ventata.add(conbobox_Panel);
    }
    private void crearComboboxAsignation(){
        JPanel conbobox_Panel2 = new JPanel();
        conbobox_Panel2.setBackground(new Color(0x84A5C9));
        conbobox_Panel2.setBounds(30, 320, 160, 30);
        conbobox_Panel2.add(acciones.getjComboBox2());
        this.ventata.add(conbobox_Panel2);
    }

    private void crearTabla(){
        JPanel panel_Table = new JPanel();
        this.listaInicial = this.cohabitantes.obtenerListaDeCohabitantes();
        String[] columnNames = {"NOMBRE","ESTADO","RAZON"};
        Object[][] datos = new Object[this.listaInicial.size()][4];
        for(int i = 0; i<this.listaInicial.size(); i++){
            datos[i]= new Object[]{this.listaInicial.get(i).getName(),this.listaInicial.get(i).getEstado().getColor(), this.listaInicial.get(i).getEstado().getRazon()};
        }
        this.dtm = new DefaultTableModel(datos, columnNames);
        this.table = new JTable(dtm);
        this.table.setPreferredScrollableViewportSize(new Dimension(410, 200));
        JScrollPane scrollPane = new JScrollPane(table);
        panel_Table.add(scrollPane, BorderLayout.CENTER);
        panel_Table.setBounds(30, 80, 410, 150);
        panel_Table.setBackground(new Color(0x84A5C9));
        this.ventata.add(panel_Table);
    }
    private void crearBotonDeAnalisis(){
        JPanel analisisPanel = new JPanel();
        analisisPanel.setBounds(30, 285, 100, 35);
        analisisPanel.setBackground(new Color(0x84A5C9));
        analisisPanel.add(this.acciones.getButtonAnalisis());
        this.ventata.add(analisisPanel);
    }
    private void crearTablaAsign(){
        JPanel panel_Table = new JPanel();
        String[] columnNames = {"FECHA", "NOMBRE DE CONDICION", "VALOR CONDICION"};
        Object[][] datos = {{"", "", ""}};
        this.dtmt = new DefaultTableModel(datos, columnNames);
        this.table2 = new JTable(dtmt);
        this.table2.setPreferredScrollableViewportSize(new Dimension(410, 200));
        JScrollPane scrollPane = new JScrollPane(this.table2);
        panel_Table.add(scrollPane, BorderLayout.CENTER);
        panel_Table.setBounds(30, 350, 410, 150);
        panel_Table.setBackground(new Color(0x84A5C9));
        this.ventata.add(panel_Table);
    }

    private void refreshTable(int idx){
        if(idx==0){
            this.listaInicial = this.cohabitantes.obtenerListaDeCohabitantes();
            resetTable(this.listaInicial);
        }else if(idx == 1){
            amarillos();
            resetTable(this.amarillos);
        }else if(idx == 2){
            rojos();
            resetTable(this.rojos);
        }else if(idx == 3){
            verdes();
            resetTable(this.verdes);
        }else if(idx == 4){
            azules();
            resetTable(this.azules);
        }
    }
    private void amarillos(){
        this.amarillos = new ArrayList<>();
        for(Cohabitante e : this.listaInicial){
            if(e.getEstado().getColor().equals("AMARILLO")){
                this.amarillos.add(e);
            }
        }
    }
    private void rojos(){
        this.rojos = new ArrayList<>();
        for(Cohabitante e : this.listaInicial){
            if(e.getEstado().getColor().equals("ROJO")){
                this.rojos.add(e);
            }
        }
    }
    private void azules(){
        this.azules = new ArrayList<>();
        for(Cohabitante e : this.listaInicial){
            if(e.getEstado().getColor().equals("AZUL")){
                this.azules.add(e);
            }
        }
    }
    private void verdes(){
        this.verdes = new ArrayList<>();
        for(Cohabitante e : this.listaInicial){
            if(e.getEstado().getColor().equals("VERDE")){
                this.verdes.add(e);
            }
        }
    }
    private void resetTable(List<Cohabitante> lista){
        String[] columnNames = {"NOMBRE","ESTADO","RAZON"};
        Object[][] datos = new Object[lista.size()][4];
        for(int i = 0; i<lista.size(); i++){
            datos[i]= new Object[]{lista.get(i).getName(),lista.get(i).getEstado().getColor(), lista.get(i).getEstado().getRazon()};
        }
        this.dtm = new DefaultTableModel(datos, columnNames);
        this.table.setModel(this.dtm);
    }
    public void bajarAlertas(int id_usuario, int id_nuevo){
        Calendar fecha = new GregorianCalendar();
        String fechaText= fecha.get(Calendar.DAY_OF_MONTH)+"-"+(fecha.get(Calendar.MONTH)+1)+"-"+fecha.get(Calendar.YEAR)+" "+fecha.get(Calendar.HOUR_OF_DAY)+":"+fecha.get(Calendar.MINUTE)+":"+fecha.get(Calendar.SECOND);
        String message_input = this.acciones.getInputDescription().getText();
        try{
            DateFormat fechaHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date convertido = fechaHora.parse(fechaText);
            if(acciones.getjComboBox2().getSelectedIndex()==0){
                this.alertas.agregarAlerta(new Alerta(id_nuevo,id_usuario, convertido,"AMARILLO", "POSIBLE POSITIVO "+message_input));
                this.cohabitantes.cambiarEstadoCohabitante(id_usuario, new EstadoCohabitante("AMARILLO", "POSIBLE POSITIVO "));
            }
            if(acciones.getjComboBox2().getSelectedIndex()==1){
                this.alertas.agregarAlerta(new Alerta(id_nuevo,id_usuario, convertido,"VERDE", "NORMAL "+message_input));
                this.cohabitantes.cambiarEstadoCohabitante(id_usuario, new EstadoCohabitante("VERDE", "NORMAL"));
            }
            if(acciones.getjComboBox2().getSelectedIndex()==2){
                this.alertas.agregarAlerta(new Alerta(id_nuevo,id_usuario,convertido,"ROJO", "POSITIVO CONFIRMADO "+message_input));
                this.cohabitantes.cambiarEstadoCohabitante(id_usuario, new EstadoCohabitante("ROJO", "TEST POSITIVO "));
            }
        }catch(ParseException parseException){
            parseException.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int id_nuevo=this.alertas.getAlertas().size()+1;
        if(e.getSource()== acciones.getButtonAnalisis()){
            int id_usuario=this.listaInicial.get(this.table.getSelectedRow()).getID();
            mostrarAnalisisDeCohabitante(id_usuario);
        }
        if(e.getSource()==acciones.getjComboBox1()) {
            refreshTable(acciones.getjComboBox1().getSelectedIndex());
        }
        if(e.getSource()==acciones.getjComboBox2()) {
            int id_usuario=this.listaInicial.get(this.table.getSelectedRow()).getID();
            bajarAlertas(id_usuario, id_nuevo);
        }
        if(e.getSource()==acciones.getButtonSelect()){
            int idx_selected = this.table.getSelectedRow();
            String label_user = mostrarSeleccion(idx_selected, acciones.getjComboBox1().getSelectedIndex());
            this.message.setText("COHABITANTE: "+label_user);
        }
        if(e.getSource()== acciones.getButtonAlertas()){
            int id_usuario=this.listaInicial.get(this.table.getSelectedRow()).getID();
            obtenerAlertasDelUsuario(id_usuario);
        }
    }
    private void llenarTablaDeCondiciones(int id) {
        List<CondicionSanitaria> condiciones_sanitarias = this.cohabitantes.obtenerListaDeCondicionesSanitarias(id);
        String[] columnNames = {"FECHA", "NOMBRE DE CONDICION", "VALOR CONDICION"};
        Object[][] datos = new Object[condiciones_sanitarias.size()][3];
        for(int i = 0; i<condiciones_sanitarias.size(); i++){
            DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String convertido = fechaHora.format(condiciones_sanitarias.get(i).getFecha());
            datos[i]= new Object[]{convertido, condiciones_sanitarias.get(i).getNombreCondicion(), condiciones_sanitarias.get(i).getValorCondicion()};
        }
        this.dtmt = new DefaultTableModel(datos, columnNames);
        this.table2.setModel(this.dtmt);
    }
    private void obtenerAlertasDelUsuario(int id) {
        List<Alerta> alertasDeUsuario = this.alertas.getAlertasxCohab(id);
        String[] columnNames = {"FECHA", "COLOR DE ALERTAS", "DESCRIPCION"};
        Object[][] datos = new Object[alertasDeUsuario.size()][4];
        for(int i = 0; i<alertasDeUsuario.size(); i++){
            DateFormat fechaHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String convertido = fechaHora.format(alertasDeUsuario.get(i).getFecha());
            datos[i]= new Object[]{convertido, alertasDeUsuario.get(i).getStatus(), alertasDeUsuario.get(i).getdescripcion()};
        }
        this.dtmt = new DefaultTableModel(datos, columnNames);
        this.table2.setModel(this.dtmt);
    }
    private String mostrarSeleccion(int idx_selected, int idx) {
        String label_user;
        String name_complet = this.listaInicial.get(idx_selected).getName();
        if(idx==0){
            label_user =  name_complet;
            llenarTablaDeCondiciones(this.listaInicial.get(idx_selected).getID());
        }else if(idx == 1){
            label_user =  name_complet;
            llenarTablaDeCondiciones(this.amarillos.get(idx_selected).getID());
        }else if(idx==2){
            label_user =  name_complet;
            llenarTablaDeCondiciones(this.rojos.get(idx_selected).getID());
        }else if(idx==3){
            label_user =  name_complet;
            llenarTablaDeCondiciones(this.verdes.get(idx_selected).getID());
        }else{
            label_user =  name_complet;
            llenarTablaDeCondiciones(this.azules.get(idx_selected).getID());
        }
        return label_user;
    }
    private void messageDates(){
        JPanel answer = new JPanel();
        answer.setBackground(Color.DARK_GRAY);
        answer.setBounds(140,245,302,71);
        answer.setLayout(new GridLayout(2,1));
        this.message = new JLabel("COHABITANTE: "+this.txt);
        message.setBackground(Color.DARK_GRAY);
        message.setForeground(Color.white);
        message.setOpaque(true);
        message.setFont(new Font("Dialog", Font.BOLD, 12));

        this.txt_analisis = new JLabel("ANALISIS: ");
        this.txt_analisis.setBackground(Color.DARK_GRAY);
        this.txt_analisis.setForeground(Color.white);
        this.txt_analisis.setOpaque(true);
        this.txt_analisis.setFont(new Font("Dialog", Font.BOLD, 12));
        answer.add(message);
        answer.add(txt_analisis);
        this.ventata.add(answer);
    }

    private void mostrarAnalisisDeCohabitante(int id_cohabitante){
        List<CondicionSanitaria> condiciones_sanitarias = this.cohabitantes.obtenerListaDeCondicionesSanitarias(id_cohabitante);
        String txt_analisis="El cohabitante esta Normal";
        DateFormat fechaHora = new SimpleDateFormat("dd");
        for (int i=0 ;i< condiciones_sanitarias.size(); i++){
            String actual = fechaHora.format(condiciones_sanitarias.get(i).getFecha());
            String siguiente;
            double siguienteTemperatura;
            if (i==condiciones_sanitarias.size()-1){
                siguiente="40";
                siguienteTemperatura=27.3;
            }else{
                siguiente = fechaHora.format(condiciones_sanitarias.get(i+1).getFecha());
                siguienteTemperatura = Double.parseDouble(condiciones_sanitarias.get(i+1).getValorCondicion());
            }
            int sig=Integer.parseInt(siguiente);
            int act=Integer.parseInt(actual);
            System.out.println(sig+" si-ac "+act);
            if((Double.parseDouble(condiciones_sanitarias.get(i).getValorCondicion()) >= 37) && (act==sig+1)){
                System.out.println("Entro al if 1ro");
                if (siguienteTemperatura>= 37 ){
                    System.out.println("Entro");
                    txt_analisis="Contacta al cohabitante para una revision";
                    break;
                }else{
                    txt_analisis="El cohabitante esta Normal";
                }
            }
        }
        this.txt_analisis.setText("ANALISIS: "+txt_analisis);
    }

    private void panelDeCredenciales(JSONObject obj){
        JPanel perfil_panel = new JPanel();
        String agente = "Agente: "+obj.get("usuario");
        JLabel asanitarioNombre_label = new JLabel(agente);
        asanitarioNombre_label.setBackground(new Color(0x84A5C9));
        asanitarioNombre_label.setOpaque(true);
        asanitarioNombre_label.setFont(new Font("Perfil", Font.BOLD,14));
        perfil_panel.add(asanitarioNombre_label);
        perfil_panel.setBackground(new Color(0x84A5C9));
        perfil_panel.setBounds(150, 33, 290, 30);
        perfil_panel.setLayout(new FlowLayout());
        this.ventata.add(perfil_panel);
    }
    private void leerConJsonSimple(){
        JSONParser parser = new JSONParser();
        try{
            Object obj = parser.parse(new FileReader("agenteConf.json"));
            JSONObject jsonObject = (JSONObject) obj;
            panelDeCredenciales(jsonObject);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
