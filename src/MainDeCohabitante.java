import pmonitoreo.backend.alertas.Alertas;
import pmonitoreo.backend.alertas.AlertasServer;
import pmonitoreo.backend.cohabitantes.Cohabitantes;
import pmonitoreo.backend.cohabitantes.CohabitantesServer;
import pmonitoreo.cohabitante.CohabitanteGUI;
import pmonitoreo.cohabitante.MovilCohabitante;

public class MainDeCohabitante {
    public static void main(String[] args) {
        Alertas alertas = new AlertasServer();
        Cohabitantes cohabitantes = new CohabitantesServer();
        MovilCohabitante CohabitanteGUI = new CohabitanteGUI(cohabitantes, alertas);
        CohabitanteGUI.iniciar();
    }
}
