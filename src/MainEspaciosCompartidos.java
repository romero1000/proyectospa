import pmonitoreo.backend.alertas.Alertas;
import pmonitoreo.backend.alertas.AlertasServer;
import pmonitoreo.backend.cohabitantes.Cohabitantes;
import pmonitoreo.backend.cohabitantes.CohabitantesServer;
import pmonitoreo.backend.espacioscompartidos.EspaciosCompartidos;
import pmonitoreo.backend.espacioscompartidos.Salas;
import pmonitoreo.bdmonitoreo.BDEspacioscompartidos;
import pmonitoreo.bdmonitoreo.EspaciosCompartidosBD;
import pmonitoreo.monitoreoweb.UIMonitoreoWeb;

public class MainEspaciosCompartidos {
    public static void main(String[] args) {
        Alertas alertas= new AlertasServer();
        Cohabitantes cohabitantes= new CohabitantesServer();
        BDEspacioscompartidos espaciosCompartidosBD= new EspaciosCompartidosBD(cohabitantes);
        EspaciosCompartidos espComp= new Salas(cohabitantes,espaciosCompartidosBD);
        UIMonitoreoWeb uiMonitoreoWeb= new UIMonitoreoWeb(espComp);
        uiMonitoreoWeb.open();

    }
}
