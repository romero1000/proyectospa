import pmonitoreo.asanitario.AgenteGUI;
import pmonitoreo.backend.alertas.Alertas;
import pmonitoreo.backend.alertas.AlertasServer;
import pmonitoreo.backend.cohabitantes.Cohabitantes;
import pmonitoreo.backend.cohabitantes.CohabitantesServer;

public class Main {
    public static void main(String []args) {
        Alertas alerta = new AlertasServer();
        Cohabitantes cohabitantes = new CohabitantesServer();
        AgenteGUI guiASanitario = new AgenteGUI(cohabitantes, alerta);
        guiASanitario.iniciar();
    }
}
